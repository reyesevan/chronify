function callMethod(method) {
    return async(req, res) => {
        let result;

        try {
            result = await method(req, res);
        } catch (e) {
            result = {
                action: "error",
                message: e.message
            }
        }

        res.send(result);
    }
};

module.exports = {
    setRoutes(app, prefix, storage) {
        app.get('/', (req, res) => {
            if (req.session.signedInUser)
                res.sendFile('home.html', { root: __dirname + "/../" })
            else
                res.sendFile('login.html', { root: __dirname + "/../" });
        })

        app.get(`${prefix}`, callMethod((req) => {
            return storage.getFor(req.session.signedInUser);
        }));

        app.post(`${prefix}`, callMethod((req) => {
            return storage.insert(req.body, req.session.signedInUser);
        }));

        app.put(`${prefix}/:id`, callMethod((req) => {
            return storage.update(req.params.id, req.body);
        }));

        app.delete(`${prefix}/:id`, callMethod((req) => {
            return storage.delete(req.params.id);
        }));
        app.get("/graph", callMethod((req, response) => {
            response.redirect('graph.html');
        }));
        app.get("/getAll", callMethod((req) => {
            return storage.getFor(req.session.signedInUser);
        }));

        app.get('/session', (req, res) => res.send(req.session))

        app.post('/login', (req, res) => {
            const username = req.body.username
            const password = req.body.password

            const result = storage.userExists(username)

            result.then(value => {
                req.session.loginUsername = username
                if (value.length) {
                    if (value[0].password == password) {
                        req.session.signedInUser = req.body.username
                        res.redirect('/')
                    }
                    else {
                        req.session.loginUsernameError = ''
                        req.session.loginPasswordError = 'Incorrect password. Try again'
                        res.sendFile('login.html', { root: __dirname + "/../" });
                    }
                }
                else {
                    req.session.loginUsernameError = 'Username do not exist'
                    req.session.loginPasswordError = ''
                    res.sendFile('login.html', { root: __dirname + "/../" });

                }
            })
        })
        
        app.post('/signup', (req, res) => {
            storage.createUser(req.body)
            req.session.signedInUser = req.body.username
            res.redirect('/')
        })

        app.post("/evaluate/", (req, res) => {
            storage.evaluate(req.body.id, req.body.eval)
        })

        app.get('/signout', (req, res) => {
            req.session.signedInUser = undefined
            res.redirect('/')
        })

        app.get("/activity/:id", (req, res) => {
            storage.getActivity(req.params.id).then((value) => {                
                const year = value[0].start_date.getFullYear()
                const date = value[0].start_date.toString().substring(0, 10) + " " + year
                const startTime = value[0].start_date.getHours() + ":" +  value[0].start_date.getMinutes()
                const endTime = value[0].end_date.getHours() + ":" + value[0].end_date.getMinutes()

                res.render("activityEvaluation", {
                    activityTitle: value[0].text,
                    startTime: startTime,
                    endTime: endTime,
                    date: date
                })
            })
        })
    }
};
$(document).ready(() => {
    getEvents()
})

$(document).on("click", "li", (e) => {
    let eventName = $(e.target).text()
})

function getEvents() {
    const currentDate = new Date()
    $.get("/getAll", (data) => {
        for (event in data) {
            const endYear = parseInt(data[event].end_date.substring(0, 4), 10)
            const endMonth = parseInt(data[event].end_date.substring(5, 7), 10)
            const endDay = parseInt(data[event].end_date.substring(8, 10), 10)
            const endHour = parseInt(data[event].end_date.substring(11, 14), 10)
            
            const startDate = data[event].start_date.substring(0, 10)
            const startTime = data[event].start_date.substring(11, 16)
            const endTime = data[event].end_date.substring(11, 16)
            

            const eventDate = new Date(endYear, endMonth-1, endDay, endHour)

            if (!data[event].evaluation && currentDate > eventDate) {
                const dateString = startDate
                const timeString = startTime + " - " + endTime
                $("#readyList").append(
                    "<li><h3><a href='activity/" + data[event].id + "'>" + 
                    data[event].text + 
                    "</h3><p>" + 
                    dateString + 
                    "<br>" + 
                    timeString + 
                    "</li>")
            }
            else if (!data[event].evaluation && currentDate < eventDate) {
                const dateString = startDate
                const timeString = startTime + " - " + endTime
                $("#notReadyList").append(
                    "<li><h3>" + 
                    data[event].text + 
                    "</h3><p>" + 
                    dateString + 
                    "<br>" + 
                    timeString + 
                    "</li>")
                
            }
            else {
                const dateString = startDate
                const timeString = startTime + " - " + endTime
                $("#pastList").append(
                    "<li><h3>" + 
                    data[event].text + 
                    "</h3><p>" + 
                    dateString + 
                    "<br>" + 
                    timeString + "<br>Productivity evaluation: " + data[event].evaluation +
                    "</li>")
            }
        }
    })
}
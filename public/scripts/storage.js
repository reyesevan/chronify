require('date-format-lite')

class Storage {
	constructor(connection) {
		this.database = connection
	}

	getData(data) {
		const id = data.ids
		const startDate = data[`${id}_start_date`]
		const endDate = data[`${id}_end_date`]
		const text = data[`${id}_text`]
		return [startDate, endDate, text]
	}

	// ANCHOR get events from the table, use dynamic loading if parameters sent
	async getAll(params) {
		let result = await this.database.query("SELECT * FROM events")

		result.rows.forEach((entry) => {
			entry.start_date = entry.start_date.format("YYYY-MM-DD hh:mm")
			entry.end_date = entry.end_date.format("YYYY-MM-DD hh:mm")
		})

		return result.rows
	}

	// ANCHOR get specific activity
	async getActivity(id) {
		const result = await this.database.query("SELECT * FROM EVENTS WHERE ID = $1", [id])
		return result.rows
	}

	// ANCHOR get all activities of an user
	async getFor(user) {
		const result = await this.database.query('SELECT * FROM EVENTS WHERE USERNAME = $1', [user])
		return result.rows
	}

	// ANCHOR update an event with its evaluation
	async evaluate(id, evaluation) {
		this.database.query("UPDATE events SET evaluation = $1 WHERE ID = $2", [evaluation, id])
		return {
			action: "updated"
		}
	}

	// ANCHOR create new event
	async insert(data, user) {
		const id = data.ids
		const action = data[`${id}_!nativeeditor_status`]

		switch (action) {
			case 'updated':
				return this.update(id, data)

			case 'deleted':
				return this.delete(id, data)
		}

		const [startDate, endDate, text] = this.getData(data)

		this.database.query(
			"INSERT INTO events VALUES ($1, $2, $3, $4, $5);",
			[id, user, startDate, endDate, text])

		return {
			action: "inserted",
		}
	}

	// ANCHOR update event
	async update(id, data) {
		const [startDate, endDate, text] = this.getData(data)

		this.database.query(
			"UPDATE events SET start_date = $1, end_date = $2, text = $3 WHERE id = $4",
			[startDate, endDate, text, id])

		return {
			action: "updated"
		}
	}

	// ANCHOR delete event
	async delete(id) {
		this.database.query("DELETE FROM events WHERE id = $1", [id])

		return {
			action: "deleted"
		}
	}

	// ANCHOR store a new user
	async createUser(data) {
		const { email, password, username } = data
		this.database.query("INSERT INTO USERS VALUES ($1, $2, $3)", [username, email, password])
	}

	// ANCHOR does the user with this username exist?
	async userExists(user) {
		const result =  await this.database.query("SELECT * FROM USERS WHERE USERNAME = $1", [user])
		return result.rows
	}
}

module.exports = Storage
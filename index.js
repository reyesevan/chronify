const express = require('express'); // use Express
const session = require('express-session')
// const bodyParser = require('body-parser'); // for parsing POST requests
const app = express(); // create application
const port = 3000; // port for listening

const {Client} = require('pg')

const router = require('./public/scripts/router')
const Storage = require('./public/scripts/storage')

const psqlConfig = {
	'user': 'postgres',
	'host': 'localhost',
	'database': 'chronify',
	'password': '',
	'port': '5432'

	// 'database': 'agendatest',
	// 'password': 'root',
	// 'port': '5433'
}

app.set('view engine', 'pug');

app.use(session({secret: '123', saveUninitialized: true, resave: true}))
// It's necessary for parsing POST requests
// the line below is used for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// app.use(express.urlencoded())

app.use(express.static(__dirname + '/public'));

// Start server...
app.listen(port, () => {
	console.log('\t╭───────────────────────────────────────────────╮')
	console.log('\t│\t\t\t\t\t\t│')
	console.log('\t│\t\t\t\t\t\t│')
	console.log('\t│\t\033[36mChronify is running on port ' + port + '...\033[0m\t│');
	console.log('\t│\t Go to \033[34mhttp://localhost:' + port + '\033[0m\t\t│');
	console.log('\t│\t\t\t\t\t\t│')
	console.log('\t│\t\t\t\t\t\t│')
	console.log('\t╰───────────────────────────────────────────────╯')
});

// Set the params to connect with the database
const client = new Client(psqlConfig)
// Create an instance of the CRUD methods for the previous database
const storage = new Storage(client)

// Establish connection with the database...
client.connect()

// Set the REST routes to perform CRUD actions over the database
router.setRoutes(app, '/events', storage)
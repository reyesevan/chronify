let week = 0

$(document).ready(() => {
  renderDashboard(week)
});

$(document).on("click", "#nextWeek", () => {
  week++
  renderDashboard(week)
})

$(document).on("click", "#previousWeek", () => {
  week--
  renderDashboard(week)
})



function renderDashboard(weekNumber) {
  $.get("/getAll", (data) => {
    const productiveHours = generateProductiveHoursMap(data)
    const chartData = generateChartData(productiveHours)
    const doughnutChartData = generateDoughnutChartData(productiveHours)
    const dayInterval = generateChartTitle()
    
    const barChart = new CanvasJS.Chart("chart", {
      backgroundColor: "#323234",
      theme: "dark2",          
      title: {
        text: dayInterval,           
      },
      data: 
      [  //array of dataSeries     
        { //dataSeries - first quarter
          /*** Change type "column" to "bar", "area", "line" or "pie"***/        
          type: "column",
          name: "Productive Time",
          showInLegend: true,
          legendText: "Productive Time",
          dataPoints: chartData["data1"]
        },
        { //dataSeries - second quarter
          type: "column",
          name: "Leisure Time",
          showInLegend: true,            
          legendText: "Leisure Time",    
          dataPoints: chartData["data2"]
        }
      ],
      axisY: {
        maximum: 1.0
      },
      animationEnabled: true,
      
    });
    barChart.render();
    
    const chart = new CanvasJS.Chart("doughnutChart",
    {
      animationEnabled: true,
      backgroundColor: "#323234",
      theme: "dark2",
      title:{
        text: "Weekly View",
        fontFamily: "Montserrat",
        fontWeight: "normal",
        fontColor: "white"
      },
      
      legend:{
        verticalAlign: "bottom",
        horizontalAlign: "center",
        fontFamily: "Montserrat",
        fontColor: "white"  
      },
      data: [
        {
          //startAngle: 45,
          indexLabelFontSize: 20,
          indexLabelFontFamily: "Montserrat",
          indexLabelFontColor: "white",
          indexLabelLineColor: "white",
          indexLabelPlacement: "outside",
          type: "doughnut",
          showInLegend: true,
          dataPoints: [
            {  y: doughnutChartData/168, legendText:"Productive Hours"},
            {  y: (168 - doughnutChartData)/168, legendText:"Leisure Hours"}
          ]
        }
      ]
    }
    );
    
    chart.render();
    
    const totalProductiveHours = doughnutChartData + " productive hours"
    const totalLeisureHours = (168 - doughnutChartData) + " leisure hours"
    const mostProductiveDay = getDayString(getMostProductiveDay(productiveHours))
    const leastProductiveDayNumber = getLeastProductiveDay(productiveHours)
    const mostProductiveDayString = mostProductiveDay + " was the most productive day"
    let leastProductiveDayString

    if (!Array.isArray(leastProductiveDayNumber)) {
      leastProductiveDayString = leastProductiveDayNumber ? getDayString(leastProductiveDayNumber) + " was the least productive day" : "You were not productive week."
    }
    else {
      leastProductiveDayString = "You were not productive " + leastProductiveDayNumber.length + " days"
    }



    $(".quickData").html("<h1>" + totalProductiveHours + "</h1>")
    $(".quickData").html("<h1>" + totalLeisureHours + "</h1>")
    
    if (mostProductiveDay) $(".quickData").append("<h2>" + mostProductiveDayString + "</h2>")
    if (leastProductiveDayNumber) $(".quickData").append("<h2>" + leastProductiveDayString + "</h2>")
    
    
  });
}

function getMostProductiveDay(productiveHoursMap) {
  let mostProductiveDay
  let highest = 0
  for (day in productiveHoursMap) {
    if (productiveHoursMap[day] > highest) {
      highest = productiveHoursMap[day]
      mostProductiveDay = day
    }
  }
  return mostProductiveDay
}

function numOfZeros(productiveHoursMap) {
  let count = 0
  for (day in productiveHoursMap) {
    if (productiveHoursMap[day] == 0) count++
  }
  return count
}

function getLeastProductiveDay(productiveHoursMap) {
  if (numOfZeros(productiveHoursMap) > 1) {
    let unproductiveDays = []

    for (day in productiveHoursMap) {
      if (productiveHoursMap[day] == 0) {
        unproductiveDays.push(day)
      }
    }

    return unproductiveDays
  }
  else {
    let leastProductiveDay
    let lowest = 1000
    for (day in productiveHoursMap) {
      if (productiveHoursMap[day] < lowest) {
        lowest = productiveHoursMap[day]
        leastProductiveDay = day
      }
    }
    return leastProductiveDay
  }
}

function getDayString(dayNumber) {
  const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
  return days[dayNumber]
}

function generateDoughnutChartData(productiveHoursMap) {
  let totalProductiveHours = 0;
  
  for (day in productiveHoursMap) {
    totalProductiveHours += productiveHoursMap[day]
  }
  
  return totalProductiveHours
}

function getDayProductiveRatio(dayOfWeek, productiveHoursMap) {
  const dayNumber = getDayNumber(dayOfWeek)
  return productiveHoursMap[dayNumber] ? productiveHoursMap[dayNumber] / 24 : 0
}

function getDayLeisureRatio(dayOfWeek, productiveHoursMap) {
  const dayNumber = getDayNumber(dayOfWeek)
  return productiveHoursMap[dayNumber] ? (24 - productiveHoursMap[dayNumber]) / 24: 1
}

function computeDayOfWeek (day, month, year) {
  const t = [0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4]
  year -= month < 3
  const dayOfWeek = (year + parseInt(year/4, 10) - parseInt(year/100, 10) + year/400 + t[month-1] + day) % 7
  return parseInt(dayOfWeek, 10)
}

function generateChartData(productiveHoursMap) {
  const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
  let data = {"data1": [], "data2": []}
  
  for (day in days) {
    data["data1"].push({label: days[day], y: getDayProductiveRatio(days[day], productiveHoursMap)})
    data["data2"].push({label: days[day], y: getDayLeisureRatio(days[day], productiveHoursMap)})
  }
  
  return data
}

function getDayNumber(dayOfWeek) {
  const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
  return days.indexOf(dayOfWeek)
}

function generateProductiveHoursMap(allEvents) {
  let productiveHours = {0: 0, 1: 0, 2:0, 3:0, 4:0, 5:0, 6:0}
  const weekStart = getWeekStartDay()
  
  for (event in allEvents) {
    const startHourShifted = new Date(allEvents[event].start_date)
    const year = parseInt(allEvents[event].start_date.substring(0, 4), 10)
    const month = parseInt(allEvents[event].start_date.substring(5, 7), 10)
    const day = parseInt(allEvents[event].start_date.substring(8, 10), 10)
    const startHour = substractHour(parseInt(allEvents[event].start_date.substring(11, 13), 10), 6)
    let endHour = substractHour(parseInt(allEvents[event].end_date.substring(11, 14), 10), 6)
    
    if (year == weekStart.getYear() + 1900 && month == weekStart.getMonth() + 1 && day >= weekStart.getDate() && day <= weekStart.getDate() + 6) {
      if (endHour == 0) endHour = 24
      const duration = Math.abs(endHour - startHour)
      console.log(startHour + " TO " + endHour)
      console.log("duration = " + endHour + "-" + startHour)
      let dayOfWeek = computeDayOfWeek(day, month, year)
      
      productiveHours[dayOfWeek] = productiveHours[dayOfWeek] ? productiveHours[dayOfWeek] + duration : duration
      
    }
  }
  return productiveHours
}

function substractHour(hour, toSubstract) {
  if (toSubstract > hour) {
    hour += 24
  }
  return hour - toSubstract
}

function getWeekStartDay() {
  let currentDate = new Date(new Date().getYear() + 1900, new Date().getMonth(), new Date().getDate() + week * 7)
  
  const weekStartDate = currentDate.getDate() - currentDate.getDay() + 1
  return new Date(currentDate.getYear() + 1900, currentDate.getMonth(), weekStartDate)
}

function generateChartTitle() {
  const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
  const weekstart = getWeekStartDay()
  const weekend = new Date(weekstart.getTime() + (6 * 24 * 60 * 60 * 1000))
  const currentMonth = months[weekstart.getMonth()]
  return currentMonth + " " + weekstart.getDate() + " - " + months[weekend.getMonth()] + " " + weekend.getDate()
}
